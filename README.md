# go-ansi-codes

![](https://gitlab.com/1H0/go-ansi-codes/badges/master/pipeline.svg)
![](https://gitlab.com/1H0/go-ansi-codes/badges/master/coverage.svg)

A quick cli tool which lists some frequently used ANSI codes written in Go.

- https://stackoverflow.com/questions/4842424/list-of-ansi-color-escape-sequences
- https://www.devdungeon.com/content/colorize-terminal-output-python
- https://github.com/fatih/color
- https://golangbyexample.com/print-output-text-color-console/
